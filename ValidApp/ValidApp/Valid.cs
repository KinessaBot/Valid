﻿using System.Text.RegularExpressions;

namespace ValidApp
{
    class Valid
    {
        public bool ValidateEmail(string email)
        {
            Match match = Regex.Match(email, @"^([a-z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-z0-9\-]+\.)+))([a-z]{2,4}|[0-9]{1,3})(\]?)$", RegexOptions.IgnoreCase);
            return match.Success;
        }

        public bool ValidateURL(string URL)
        {
            Match match = Regex.Match(URL, @"^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$", RegexOptions.IgnoreCase);
            return match.Success;
        }

        public bool ValidatFilePath(string path)
        {
            string regex = @"^([a-zA-Z]\:|\\\\[^\/\\:*?"" <>|]+\\[^\/\\:*?""<>|]+)(\\[^\/\\:*?""<>|]+)+(\.[^\/\\:*?""<>|]+)$";
            Match match = Regex.Match(path, regex , RegexOptions.IgnoreCase);
            return match.Success;
        }
    }
}
